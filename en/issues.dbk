<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
"https://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
<!ENTITY % languagedata SYSTEM "language.ent" > %languagedata;
<!ENTITY % shareddata   SYSTEM "../release-notes.ent" > %shareddata;
]>

<chapter id="ch-information" lang="en">
  <title>Issues to be aware of for &releasename;</title>

  <para>
    Sometimes, changes introduced in a new release have side-effects
    we cannot reasonably avoid, or they expose
    bugs somewhere else. This section documents issues we are aware of.  Please also
    read the errata, the relevant packages' documentation, bug reports, and other
    information mentioned in <xref linkend="morereading"/>.
  </para>

  <section id="upgrade-specific-issues">
    <title>Upgrade specific items for &releasename;</title>
    <para>
      This section covers items related to the upgrade from
      &oldreleasename; to &releasename;.
    </para>

    <section id="non-free-split">
      <!-- bullseye to bookworm -->
      <title>
	Non-free firmware moved to its own component in the archive
      </title>
      <para>
	As described in <xref linkend="archive-areas"/>, non-free
	firmware packages are now served from a dedicated archive
	component, called <literal>non-free-firmware</literal>. To
	ensure installed non-free firmware packages receive proper
	upgrades, changes to the APT configuration are
	required. Assuming the <literal>non-free</literal> component
	was only added to the APT sources-list to install firmware,
	the updated APT source-list entry could look like:
      </para>
	
      <programlisting>deb https://deb.debian.org/debian bookworm main non-free-firmware</programlisting>

      <para>
	If you were pointed to this chapter by <command>apt</command>
	you can prevent it from continuously notifying you about this
	change by creating an <ulink
	url="&url-man;/&releasename;/apt/apt.conf.5.html">apt.conf(5)</ulink>
	file named
	<filename>/etc/apt/apt.conf.d/no-bookworm-firmware.conf</filename>
	with the following content:
      </para>

      <programlisting>APT::Get::Update::SourceListWarnings::NonFreeFirmware "false";</programlisting>

    </section>

    <section id="puppetserver">
      <!-- bullseye to bookworm -->
      <title>Puppet configuration management system upgraded to 7</title>
      <para>
	Puppet has been upgraded from 5 to 7, skipping the Puppet 6
	series altogether. This introduces major changes to the Puppet
	ecosystem.
      </para>
      <para>
        The classic Ruby-based Puppet Master 5.5.x application has
        been deprecated upstream and is no longer available in Debian.
        It is replaced by Puppet Server 7.x, provided by the
        <systemitem role="package">puppetserver</systemitem> package. The
        package is automatically installed as a dependency of the
        transitional <systemitem role="package">puppet-master</systemitem> package.
      </para>
      <para>
        In some cases, Puppet Server is a drop-in replacement for
        Puppet Master, but you should review the configuration files
        available under <literal>/etc/puppet/puppetserver</literal> to
        ensure the new defaults are suitable for your deployment. In
        particular the legacy format for the
        <literal>auth.conf</literal> file is deprecated, see the
        <ulink
        url="https://www.puppet.com/docs/puppet/7/server/config_file_auth.html">auth.conf
        documentation</ulink> for details.
      </para>
      <para>
        The recommended approach is to upgrade the server before
        clients. The Puppet 7 Server is <ulink
        url="https://www.puppet.com/docs/puppet/7/server/compatibility_with_puppet_agent.html">
        backwards compatible with older clients</ulink>; a Puppet 5
        Server can still handle upgraded agents but cannot register
        new Puppet 7 agents. So if you deploy new Puppet 7 agents
        before upgrading the server, you will not be able to add them
        to the fleet.
      </para>
      <para>
        The <systemitem role="package">puppet</systemitem>
        package has been replaced by the <systemitem
        role="package">puppet-agent</systemitem> package and is now a
        transitional package to ensure a smooth upgrade.
      </para>
      <para>
        Finally, the <systemitem role="package">puppetdb</systemitem>
        package was removed in bullseye but is reintroduced in
        bookworm.
      </para>
    </section>

    <section id="youtube-dl">
      <title>youtube-dl replaced with yt-dlp</title>
      <para>
        The popular tool <systemitem role="package">youtube-dl</systemitem>,
	which can download videos from
	a large variety of websites (including, but not limited to, YouTube)
	is no longer included in &debian;. Instead,
	it has been replaced with an empty transitional package that pulls in the
	<systemitem role="package">yt-dlp</systemitem> package instead.
	<systemitem role="package">yt-dlp</systemitem> is a fork of
	<systemitem role="package">youtube-dl</systemitem> where new development
	is currently happening.
      </para>
      <para> 
	There are no compatibility wrappers provided, so you'll
	need to modify your scripts and personal behavior to call
	<command>yt-dlp</command> instead of <command>youtube-dl</command>.
	The functionality should be mostly the same, although some options and
	behavioral details have changed. Be sure to check
        <command>yt-dlp</command>'s <ulink
        url="&url-man;/&releasename;/yt-dlp/yt-dlp.1.html">man page</ulink>
        for details,
	and in particular the
	<ulink url="&url-man;&releasename;/yt-dlp/yt-dlp.1.html#Differences_in_default_behavior">Differences
	in default behavior</ulink> section.
     </para>
    </section>

    <section id="fcitx-fcitx5-bookworm-conflict">
      <title>Fcitx versions no longer co-installable</title>
      <para>
        The packages <systemitem role="package">fcitx</systemitem> and
        <systemitem role="package">fcitx5</systemitem> provide version 4 and
        version 5 of the popular Fcitx Input Method Framework. Following
        upstream's recommendation, they can no longer be co-installed
        on the same operating system. Users should determine which version of
        Fcitx is to be kept if they had co-installed <systemitem role="package">fcitx</systemitem>
        and <systemitem role="package">fcitx5</systemitem> previously.
      </para>
      <para>
        Before the upgrade, users are strongly encouraged to purge all related 
        packages for the unwanted Fcitx version (<literal>fcitx-*</literal> for
        Fcitx 4, and <literal>fcitx5-*</literal> for Fcitx 5). When the upgrade
        is finished, consider executing the <command>im-config</command> again
        to select the desired input method framework to be used in the system.
      </para>
      <para>
        You can read more background information in
        <ulink url="https://lists.debian.org/debian-chinese-gb/2021/12/msg00000.html">
        the announcement posted in the mailing list</ulink> (text
        written in Simplified Chinese).
      </para>
    </section>

    <section id="changes-to-system-logging">
      <title>Changes to system logging</title>
      <para>
        The <systemitem role="package">rsyslog</systemitem> package is
        no longer needed on most systems and you may be able to remove
        it.
      </para>
      <para>
        Many programs produce log messages to inform the user of what
        they are doing. These messages can be managed by systemd's
        <quote>journal</quote> or by a <quote>syslog daemon</quote>
        such as <literal>rsyslog</literal>.
      </para>
      <para>
        In &oldreleasename;, <systemitem
        role="package">rsyslog</systemitem> was installed by default
        and the systemd journal was configured to forward log messages
        to rsyslog, which writes messages into various text files such
        as <filename>/var/log/syslog</filename>.
      </para>
      <para>
        From &releasename;, <systemitem
        role="package">rsyslog</systemitem> is no longer installed by
        default. If you do not want to continue using
        <literal>rsyslog</literal>, after the upgrade you can mark it
        as automatically installed with
        <programlisting>apt-mark auto rsyslog</programlisting>
        and then an
        <programlisting>apt autoremove</programlisting>
        will remove it, if possible. If you have
        upgraded from older Debian releases, and not accepted the
        default configuration settings, the journal may not have been
        configured to save messages to persistent storage:
        instructions for enabling this are in <ulink
        url="&url-man;/&releasename;/systemd/journald.conf.5.html">journald.conf(5)</ulink>.
      </para>
      <para>
        If you decide to switch away from <systemitem
        role="package">rsylog</systemitem> you can use the
        <command>journalctl</command> command to read log messages,
        which are stored in a binary format under
        <filename>/var/log/journal</filename>.  For example,
        <programlisting>journalctl -e</programlisting> shows the most recent log
        messages in the journal and <programlisting>journalctl -ef</programlisting>
        shows new messages as they are written (similar to running
        <programlisting>tail -f /var/log/syslog</programlisting>).
      </para>
    </section>

    <section id="rsyslog-timestamp-change-affects-logcheck">
      <title>rsyslog changes affecting log analyzers such as logcheck</title>

      <para><systemitem role="package">rsyslog</systemitem> now
      defaults to <quote>high precision timestamps</quote> which may
      affect other programs that analyze the system logs. There is
      further information about how to customize this setting in
      <ulink
        url="&url-man;/&releasename;/rsyslog/rsyslog.conf.5.html">rsyslog.conf(5)</ulink>.
      </para>

      <para>The change in timestamps may require locally-created
      <systemitem role="package">logcheck</systemitem> rules to be
      updated. <literal>logcheck</literal> checks messages in the
      system log (produced by <literal>systemd-journald</literal> or
      <literal>rsyslog</literal>) against a customizable database of
      regular expressions known as rules. Rules that match the time
      the message was produced will need to be updated to match the
      new <literal>rsyslog</literal> format. The default rules, which are provided by the
      <systemitem role="package">logcheck-database</systemitem>
      package, have been updated, but other rules, including those
      created locally, may require updating to recognize the new
      format. See <ulink url="https://salsa.debian.org/debian/logcheck/-/blob/debian/1.4.0/debian/logcheck-database.NEWS">/usr/share/doc/logcheck-database/NEWS.Debian.gz</ulink>
      for a script to help update local <literal>logcheck</literal>
      rules.</para>

    </section>

    <section id="rsyslog-creates-fewer-logfiles">
      <title>rsyslog creates fewer log files</title>
      <para>
        <systemitem role="package">rsyslog</systemitem> has changed which log files it
        creates, and some files in <filename>/var/log</filename> can be
        deleted.
      </para>

      <para>
        If you are continuing to use <systemitem
        role="package">rsyslog</systemitem> (see <xref linkend="changes-to-system-logging"/>), some log
        files in <literal>/var/log</literal> will no longer be created
        by default. The messages that were written to these files are
        also in <literal>/var/log/syslog</literal> but are no longer
        created by default. Everything that used to be written to
        these files will still be available in
        <literal>/var/log/syslog</literal>.
      </para>
      <para>The files that are no longer created are:
        <itemizedlist>
          <listitem>
            <para><literal>/var/log/mail.{info,warn,err,log}*</literal></para>
            <para>
              These files contained messages from the local mail
              transport agent (MTA).
            </para>
            <para>
              Whether you can delete these depends on which MTA
              you have installed. If you were
              using <systemitem role="package">exim4</systemitem>, the
              default MTA, these files can be deleted. Other MTAs may
              still create some of these files - consult the
              documentation in <literal>/usr/share/doc</literal>.
            </para>
          </listitem>
          <listitem>
            <para><literal>var/log/lpr.log*</literal></para>
            <para>
              These files contained log messages relating to
              printing. The default print system in debian is
              <systemitem role="package">cups</systemitem> which does
              not use this file, so unless you installed a different
              printing system these files can be deleted.
            </para>
          </listitem>
          <listitem>
            <para><literal>/var/log/{messages,debug,daemon}*</literal></para>
            <para>
              These files can be deleted. Everything that used to be
              written to these files will still be in
              <literal>/var/log/syslog</literal>.
            </para>
          </listitem>
        </itemizedlist>
      </para>
    </section>

    <section id="before-first-reboot">
      <title>Things to do post upgrade before rebooting</title>
      <!-- If there is nothing to do -->
      <para>
	When <literal>apt full-upgrade</literal> has finished, the
	<quote>formal</quote> upgrade is complete.  For the upgrade to
	&releasename;, there are no special actions needed before
	performing a reboot.
      </para>
      <!-- If there is something to do -->
      <para condition="fixme">
	When <literal>apt full-upgrade</literal> has finished, the <quote>formal</quote> upgrade
	is complete, but there are some other things that should be taken care of
	<emphasis>before</emphasis> the next reboot.
      </para>

      <programlisting condition="fixme">
	add list of items here
	<!--itemizedlist>
            <listitem>
            <para>
            bla bla blah
            </para>
            </listitem>
	    </itemizedlist-->
      </programlisting>
    </section>
  </section>

  <section id="not-upgrade-only">
    <title>Items not limited to the upgrade process</title>
    <section id="limited-security-support">
      <title>Limitations in security support</title>
      <para>
	There are some packages where Debian cannot promise to provide
	minimal backports for security issues.  These are covered in the
	following subsections.
      </para>
      <note>
	<para>
	  The package <systemitem
	  role="package">debian-security-support</systemitem> helps to track the
	  security support status of installed packages.
	</para>
      </note>

      <section id="browser-security">
	<!-- Check if this still matches the view of the security team -->
	<title>Security status of web browsers and their rendering engines</title>
	<para>
	  Debian &release; includes several browser engines which are affected by a
	  steady stream of security vulnerabilities. The high rate of
	  vulnerabilities and partial lack of upstream support in the form of long
	  term branches make it very difficult to support these browsers and
	  engines with backported security fixes.  Additionally, library
	  interdependencies make it extremely difficult to update to newer upstream
	  releases. Therefore, browsers built upon e.g. the webkit and khtml
	  engines<footnote><para>These engines are shipped in a number of different
	  source packages and the concern applies to all packages shipping
	  them. The concern also extends to web rendering engines not explicitly
	  mentioned here, with the exception of webkit2gtk and the new
	  wpewebkit.</para></footnote> are included in &releasename;, but not
	  covered by security support. These browsers should not be used against
	  untrusted websites.
	  The webkit2gtk and wpewebkit engines <emphasis>are</emphasis>
	  covered by security support.
	</para>
	<para>
	  For general web browser use we recommend Firefox or Chromium.
	  They will
	  be kept up-to-date by rebuilding the current ESR releases for
	  stable.
	  The same strategy will be applied for Thunderbird.
	</para>
	<para>
	  Once a release becomes <literal>oldstable</literal>, officially supported
	  browsers may not continue to receive updates for the standard period of
	  coverage. For example, Chromium will only receive 6 months of security
	  support in <literal>oldstable</literal> rather than the typical
	  12 months.
	</para>
      </section>

      <section id="openjdk-21">
	<!-- Expecting to have something similar for trixie -->
	<title>OpenJDK 21</title>
	<para>
	  Debian &releasename; comes with an early access version of
	  <literal>OpenJDK 21</literal> (the next expected
	  <literal>OpenJDK LTS</literal> version after <literal>OpenJDK
	  17</literal>), to avoid the rather tedious bootstrap
	  process. The plan is for <literal>OpenJDK 21</literal> to
	  receive an update in &releasename; to the final upstream release
	  announced for September 2023, followed by security updates on a
	  best effort basis, but users should not expect to see updates
	  for every quarterly upstream security update.
	</para>
      </section>

      <section id="golang-static-linking" condition="fixme">
	<!-- Check if this still matches the view of the security team -->
	<title>Go- and Rust-based packages</title>
	<para>
	  The Debian infrastructure currently has problems with
	  rebuilding packages of types that systematically use static
	  linking. Before buster this wasn't a problem in practice,
	  but with the growth of the Go and Rust ecosystems it means that
	  these packages will be covered by limited security
	  support until the infrastructure is improved to deal with
	  them maintainably.
	</para>
	<para>
	  If updates are warranted for Go or Rust development libraries,
	  they can only come via regular point releases, which may be
	  slow in arriving.
	</para>
      </section>
    </section>

    <section id="python3-pep-668">
      <title>Python Interpreters marked externally-managed</title>
      <para>
	The Debian provided python3 interpreter packages
	(<systemitem role="package">python3.11</systemitem> and
	<systemitem role="package">pypy3</systemitem>)
	are now marked as being externally-managed, following
	<ulink url="https://peps.python.org/pep-0668/">PEP-668</ulink>.
	The version of <systemitem role="package">python3-pip</systemitem>
	provided in Debian follows this, and will refuse to manually install
	packages on Debian's python interpreters, unless the
	<literal>--break-system-packages</literal> option is specified.
      </para>
      <para>
	If you need to install a Python application (or version) that isn't
	packaged in Debian, we recommend that you install it with
	<command>pipx</command> (in the
	<systemitem role="package">pipx</systemitem> Debian package).
	<command>pipx</command> will set up an environment isolated from other
	applications and system Python modules, and install the application and
	its dependencies into that.
      </para>
      <para>
	If you need to install a Python library module (or version) that isn't
	packaged in Debian, we recommend installing it into a virtualenv, where
	possible. You can create virtualenvs with the <literal>venv</literal>
	Python stdlib module (in the
	<systemitem role="package">python3-venv</systemitem> Debian package) or
	the <command>virtualenv</command> Python 3rd-party tool (in the
	<systemitem role="package">virtualenv</systemitem> Debian package). For
	example, instead of running
	<command>pip install --user <replaceable>foo</replaceable></command>, run:
	<command>
	  mkdir -p ~/.venvs &amp;&amp;
	  python3 -m venv ~/.venvs/<replaceable>foo</replaceable> &amp;&amp;
	  ~/.venvs/<replaceable>foo</replaceable>/bin/python -m pip install <replaceable>foo</replaceable>
	</command> to install it in a dedicated virtualenv.
      </para>
      <para>
	See <filename>/usr/share/doc/python3.11/README.venv</filename> for more
	details.
      </para>
    </section>

    <section id="dummy" condition="fixme">
      <!-- bullseye to bookworm -->
      <title>Something</title>
      <para>
	Text.
      </para>
    </section>

  </section>

  <section id="obsolescense-and-deprecation">
    <title>Obsolescence and deprecation</title>
    <section id="noteworthy-obsolete-packages">
      <title>Noteworthy obsolete packages</title>
      <para>
	The following is a list of known and noteworthy obsolete
	packages (see <xref linkend="obsolete"/> for a description).
	<programlisting condition="fixme">
          TODO: Use the change-release information and sort by popcon

          This needs to be reviewed based on real upgrade logs (jfs)

          Alternative, another source of information is the UDD
          'not-in-testing' page:
          https://udd.debian.org/bapase.cgi?t=testing
	</programlisting>
      </para>
      <para>
	The list of obsolete packages includes:
	<itemizedlist>

          <listitem>
            <para>
              The <systemitem role="package">libnss-ldap</systemitem> package
              has been removed from &releasename;. Its functionalities are
              now covered by <systemitem role="package">libnss-ldapd</systemitem>
              and <systemitem role="package">libnss-sss</systemitem>.
            </para>
            <para>
              The <systemitem role="package">libpam-ldap</systemitem> package
              has been removed from &releasename;. Its replacement is
              <systemitem role="package">libpam-ldapd</systemitem>.
            </para>
            <para>
              The <systemitem role="package">fdflush</systemitem> package
              has been removed from &releasename;. In its stead, please
              use <command>blockdev --flushbufs</command> from
              <systemitem role="package">util-linux</systemitem>.
            </para>
          </listitem>

	</itemizedlist>
      </para>
    </section>

    <section id="deprecated-components">
      <title>Deprecated components for &releasename;</title>
      <para>
	With the next release of &debian; &nextrelease; (codenamed
	&nextreleasename;) some features will be deprecated. Users
	will need to migrate to other alternatives to prevent
	trouble when updating to &debian; &nextrelease;.
      </para>
      <para>
	This includes the following features:
      </para>

      <itemizedlist>

	<listitem>
          <para>
            Development of the NSS service <literal>gw_name</literal>
            stopped in 2015. The associated package
            <systemitem role="package">libnss-gw-name</systemitem>
            may be removed in future &debian; releases.
            The upstream developer suggests using
            <systemitem role="package">libnss-myhostname</systemitem> instead.
          </para>
          <para>
            <systemitem role="package">dmraid</systemitem> has not
            seen upstream activity since end 2010 and has been on life
            support in &debian;. bookworm will be the last release to
            ship it, so please plan accordingly if you're using
            <systemitem role="package">dmraid</systemitem>.
          </para>
	</listitem>

      </itemizedlist>
    </section>

    <section id="no-longer-supported-hardware" condition="fixme">
      <title>No-longer-supported hardware</title>
      <para>
	For a number of `arch`-based devices that were supported in
	&oldreleasename;, it is no longer viable for Debian to build the
	required <literal>Linux</literal> kernel, due to hardware
	limitations. The unsupported devices are:
      </para>
      <itemizedlist>
	<listitem>
	  <para>
	    foo
	  </para>
	</listitem>
      </itemizedlist>
      <para>
	Users of these platforms who wish to upgrade to &releasename;
	nevertheless should keep the &oldreleasename; APT sources
	enabled. Before upgrading they should add an APT preferences
	file containing:
	<programlisting condition="fixme">
Package: linux-image-marvell
Pin: release n=&oldreleasename;
Pin-Priority: 900
	</programlisting>
	The security support for this configuration will only last
	until &oldreleasename;'s End Of Life.
      </para>
    </section>
  </section>

  <section id="rc-bugs" condition="fixme">
    <title>Known severe bugs</title>
    <para>
      Although Debian releases when it's ready, that unfortunately
      doesn't mean there are no known bugs. As part of the release
      process all the bugs of severity serious or higher are actively
      tracked by the Release Team, so an <ulink
      url="&url-bts;cgi-bin/pkgreport.cgi?users=release.debian.org@packages.debian.org;tag=&releasename;-can-defer">overview
      of those bugs</ulink> that were tagged to be ignored in the last
      part of releasing &releasename; can be found in the <ulink
      url="&url-bts;">Debian Bug Tracking System</ulink>. The
      following bugs were affecting &releasename; at the time of the
      release and worth mentioning in this document:
    </para>
    <informaltable pgwide="1">
      <tgroup cols="3">
	<colspec align="justify"/>
	<colspec align="justify"/>
	<colspec align="justify"/>
	<thead>
	  <row>
	    <entry>Bug number</entry>
	    <entry>Package (source or binary)</entry>
	    <entry>Description</entry>
	  </row>
	</thead>
	<tbody>

          <row>
            <entry><ulink url="&url-bts;922981">922981</ulink></entry>
            <entry><systemitem role="package">ca-certificates-java</systemitem></entry>
            <entry>ca-certificates-java: /etc/ca-certificates/update.d/jks-keystore doesn't update /etc/ssl/certs/java/cacerts</entry>
          </row>

	</tbody>
      </tgroup>
    </informaltable>
  </section>
</chapter>
